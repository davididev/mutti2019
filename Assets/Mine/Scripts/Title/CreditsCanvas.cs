﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsCanvas : MonoBehaviour
{
    public TextAsset creditsFile;
    public TMPro.TextMeshProUGUI textObj;

    private bool started = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CycleCredits());
    }

    private void OnBecameVisible()
    {
        
    }

    IEnumerator CycleCredits()
    {
        started = true;
        string[] sections = creditsFile.text.Split('*');
        while(gameObject)
        {
            for(int i = 0; i < sections.Length; i++)
            {
                textObj.text = "---Credits---\n" + sections[i];
                yield return new WaitForSeconds(10f);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
