﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingUI : MonoBehaviour
{
    public TMPro.TextMeshProUGUI loadingText;
    public static int SceneIDToLoad = 1;
    // Start is called before the first frame update
    void Start()
    {
        //Update saved level
        int levelID = SceneIDToLoad - 2;
        int savedLevel = PlayerPrefs.GetInt("LevelsUnlocked");
        if (levelID > savedLevel)
        {
            PlayerPrefs.SetInt("LevelsUnlocked", levelID);
            PlayerPrefs.Save();
        }

        //Show the loading
        StartCoroutine(SiSenorGracious());
    }

    IEnumerator SiSenorGracious()
    {
        Time.timeScale = 1f;
        PlayerMovement.disablePause = true;
        AsyncOperation lolwut = SceneManager.LoadSceneAsync(SceneIDToLoad);
        if (lolwut == null) //Scene invalid; load title
        {
            int rate = PlayerPrefs.GetInt("RateMe");
            if (rate == 0)
                PlayerPrefs.SetInt("RateMe", 1);
            lolwut = SceneManager.LoadSceneAsync(0);
        }
        while (lolwut.isDone == false)
        {
            loadingText.text = "Loading: \n" + Mathf.RoundToInt(lolwut.progress * 100f) + "%";
            yield return new WaitForEndOfFrame();
        }
        PlayerMovement.disablePause = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
