﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectionCanvas : MonoBehaviour
{
    public Transform levelContainer;
	public GameObject ratePanel;

    // Start is called before the first frame update
    void Start()
    {
        int levels = PlayerPrefs.GetInt("LevelsUnlocked");
        int children = levelContainer.childCount;
        for(int i = 0; i < children; i++)
        {
            levelContainer.GetChild(i).gameObject.SetActive(levels >= i);
        }

        Invoke("StartUI", 0.1f);
    }

    void StartUI()
    {
        PlayerHand.uiMode = true;
		if(PlayerPrefs.GetInt("RateMe") == 1)
		{
			ratePanel.SetActive(true);
		}
    }
	
	public void GoToGooglePlay()
	{
		Application.OpenURL("https://play.google.com/store/apps/details?id=com.Wts.Mutti2019");
	}

    public void PlayLevelID(int id)
    {
        PlayerHand.uiMode = false;
        LoadingUI.SceneIDToLoad = 2 + id;
        SceneManager.LoadScene("Loading");
    }

    public void QuitGame()
    {
        Debug.Log("There is no way I'd rather quit!");
        Application.Quit();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
