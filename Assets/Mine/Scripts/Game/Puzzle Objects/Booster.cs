﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : MonoBehaviour
{
	public float pushFrc = 20f;
	public bool isTurnedOnAutomatically = false;
	public ParticleSystem ps;
	public AudioClip boostSoundFX;
	
	private bool turnedOn;
	
    // Start is called before the first frame update
    void Start()
    {
        TurnOff();
    }
	
	//Called by SendMessage
	void TurnOn()
	{
		turnedOn = !isTurnedOnAutomatically;
		EnableOrDisableParticleSystem();
	}
	
	//Called by SendMessage
	void TurnOff()
	{
		turnedOn = isTurnedOnAutomatically;
		EnableOrDisableParticleSystem();
	}
	
	
	void EnableOrDisableParticleSystem()
	{
		if(turnedOn)
			ps.Play();
		else
			ps.Stop();
	}

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player" && turnedOn == true)
		{
			c.GetComponent<CharacterControllerMovement>().AddForce(transform.up * pushFrc);
			Finch.FinchController.Right.HapticPulse(50);
			AudioSource.PlayClipAtPoint(boostSoundFX, transform.position);
		}
	}
}
