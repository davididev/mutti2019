﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CButton : MonoBehaviour
{
    public AudioClip buttonDown, buttonUp;
    public CCube.CColor color = CCube.CColor.RED;
    public GameObject[] effectingObjects;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Cube")
        {
            if (color == other.GetComponent<CCube>().cubeColor)
            {
                if (anim == null)
                    anim = GetComponent<Animator>();

                anim.SetBool("Pressed", true);
                foreach (GameObject g in effectingObjects)
                {
                    g.SendMessage("TurnOn");

                }

                AudioSource.PlayClipAtPoint(buttonDown, transform.position);
                Finch.FinchController.Right.HapticPulse(150);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Cube")
        {
            if (color == other.GetComponent<CCube>().cubeColor)
            {
                if (anim == null)
                    anim = GetComponent<Animator>();

                anim.SetBool("Pressed", false);
                foreach (GameObject g in effectingObjects)
                {
                    g.SendMessage("TurnOff");

                }
                AudioSource.PlayClipAtPoint(buttonUp, transform.position);
                Finch.FinchController.Right.HapticPulse(50);
            }
        }
    }

}
