﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCube : MonoBehaviour
{
    public enum CColor { RED, GREEN, BLUE };
    public CColor cubeColor = CColor.RED;
    public AudioClip clangSound;
    private Vector3 startingPosition;
    public GameObject teleportPrefab;
    private GameObject teleportInstance;
    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        teleportInstance = GameObject.Instantiate(teleportPrefab) as GameObject;
        teleportInstance.SetActive(false);
    }

    public void PossibleTeleport(CColor barrierColor)
    {
        if(cubeColor == barrierColor)
        {
            teleportInstance.transform.position = transform.position;
            teleportInstance.SetActive(true);

            transform.position = startingPosition;
            transform.parent = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    public void OpenTrigger()
    {
        GetComponentInChildren<MeshRenderer>().material.color = new Color(1f, 1f, 1f, 1f);
    }

    public void CloseTrigger()
    {
        GetComponentInChildren<MeshRenderer>().material.color = new Color(0.6f, 0.6f, 0.6f, 1f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Default"))
            AudioSource.PlayClipAtPoint(clangSound, transform.position);
    }
}
