﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public const float ROTATE_PER_SECOND = 90f;
    public Transform portalToGoTo;
    public AudioClip teleportSound;

    public static bool teleporting = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rot = transform.eulerAngles;
        rot.z += ROTATE_PER_SECOND * Time.deltaTime;
        transform.eulerAngles = rot;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (teleporting == true)
            return;
        if(other.tag == "Player")
        {
            teleporting = true;
            StartCoroutine(TeleportWait());
            CharacterController cc = other.GetComponent<CharacterController>();
            cc.enabled = false;
            other.transform.position = portalToGoTo.position;
            AudioSource.PlayClipAtPoint(teleportSound, portalToGoTo.position);
            cc.enabled = true;
            Finch.FinchController.Right.HapticPulse(500);
        }
    }

    //Wait four framesteps before you can teleport again
    IEnumerator TeleportWait()
    {
        for(int i = 0; i < 4; i++)
        {
            yield return new WaitForFixedUpdate();
        }

        teleporting = false;
    }
}
