﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeHole : MonoBehaviour
{
    public CCube.CColor color = CCube.CColor.RED;
    public GameObject[] piecesToEffect;
    public AudioClip successSoundEffet;
    public TextAsset successDialogue;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Cube")
        {
            if(color == other.GetComponent<CCube>().cubeColor)
            {
                //Send messages to the puzzle objects, such as trails and doors
                foreach(GameObject g in piecesToEffect)
                {
                    g.SendMessage("TurnOn");  //Pieces that effect should have a TurnOn and TurnOff button
                }
                AudioSource.PlayClipAtPoint(successSoundEffet, transform.position);
                //Lock the cube in place
                other.transform.position = transform.position;
                Rigidbody rg = other.GetComponent<Rigidbody>();
                rg.isKinematic = true;
                rg.useGravity = false;
                Finch.FinchController.Right.HapticPulse(150);

                if (successDialogue != null)
                    FindObjectOfType<DialogueHandler>().StartDialogue(successDialogue);
            }
        }
    }
}
