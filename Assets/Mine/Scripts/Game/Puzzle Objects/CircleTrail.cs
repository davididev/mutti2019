﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleTrail : MonoBehaviour
{
    public Texture offTrail, onTrail;

    public void TurnOn()
    {
        GetComponent<MeshRenderer>().material.mainTexture = onTrail;
    }

    public void TurnOff()
    {
        GetComponent<MeshRenderer>().material.mainTexture = offTrail;
    }
    
}
