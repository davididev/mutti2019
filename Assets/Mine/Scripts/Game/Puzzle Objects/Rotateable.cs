﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotateable : MonoBehaviour
{
    public Vector3 rotateBy;
    public float rotatePerSecond = 180f;
    public AudioClip rotateSound;
    private Vector3 startingRotate, secondRotate, targetRotate;

    // Start is called before the first frame update
    void Start()
    {
        startingRotate = transform.eulerAngles;
        secondRotate = startingRotate + rotateBy;
        targetRotate = startingRotate;


    }

    public void TurnOn()
    {
        targetRotate = secondRotate;
        AudioSource.PlayClipAtPoint(rotateSound, transform.position);
    }

    public void TurnOff()
    {
        targetRotate = startingRotate;
        AudioSource.PlayClipAtPoint(rotateSound, transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rot = transform.eulerAngles;
        rot.x = Mathf.MoveTowardsAngle(rot.x, targetRotate.x, rotatePerSecond * Time.deltaTime);
        rot.y = Mathf.MoveTowardsAngle(rot.y, targetRotate.y, rotatePerSecond * Time.deltaTime);
        rot.z = Mathf.MoveTowardsAngle(rot.z, targetRotate.z, rotatePerSecond * Time.deltaTime);
        transform.eulerAngles = rot;
    }
}
