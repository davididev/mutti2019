﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBarrier : MonoBehaviour
{
    private CCube.CColor currentColor;
    public CCube.CColor barrierColor = CCube.CColor.RED, barrierTurnedOnColor = CCube.CColor.GREEN;
    public Material offMaterial, onMaterial;
    private MeshRenderer rend;
    private const float X_VEC = 0.4f, Y_VEC = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        currentColor = barrierColor;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Cube")
        {
            other.GetComponent<CCube>().PossibleTeleport(currentColor);
        }
    }

    public void TurnOn()
    {
        currentColor = barrierTurnedOnColor;
        GetComponent<MeshRenderer>().material = onMaterial;
    }

    public void TurnOff()
    {
        currentColor = barrierColor;
        GetComponent<MeshRenderer>().material = offMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        if (rend == null)
            rend = GetComponent<MeshRenderer>();


        Vector2 v = rend.material.mainTextureOffset;
        v.x += X_VEC * Time.deltaTime;
        v.y += Y_VEC * Time.deltaTime;

        rend.material.mainTextureOffset = v;
    }
}
