﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Elevator : MonoBehaviour
{
    private Rigidbody rigid;
    public enum DIRECTION { Up, Forward, Right };
    public DIRECTION elevatorDirection = DIRECTION.Up;
    public float maxRange = 10f, minRange = -10f, moveSpeed = 0.5f;
    private Vector3 startingPosition;

    public bool turnedOnByDefault = false;
    private bool turnedOn, currentGoingTowardsMax = true;

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        
        rigid = GetComponent<Rigidbody>();
        if (elevatorDirection == DIRECTION.Up)
            rigid.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        if (elevatorDirection == DIRECTION.Forward)
            rigid.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        if (elevatorDirection == DIRECTION.Right)
            rigid.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;

        TurnOff();
    }

    public void TurnOn()
    {
        turnedOn = !turnedOnByDefault;
        GetComponent<Rigidbody>().isKinematic = turnedOnByDefault;
        Debug.Log("Elevator on.  Turned on: " + turnedOn);
    }

    public void TurnOff()
    {
        turnedOn = turnedOnByDefault;

        GetComponent<Rigidbody>().isKinematic = turnedOnByDefault;

        Debug.Log("Elevator off.  Turned on: " + turnedOn);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rigid == null)
            GetComponent<Rigidbody>();

        if (turnedOn)
        {
            //Going up/down
            if (elevatorDirection == DIRECTION.Up)
            {
                
                if (currentGoingTowardsMax == true)
                {
                    //Debug.Log("Going up!");
                    rigid.velocity = Vector3.up * moveSpeed;
                    if (transform.position.y > (startingPosition.y + maxRange))
                        currentGoingTowardsMax = false;
                }
                else
                {
                    //Debug.Log("Going down!");
                    rigid.velocity = Vector3.down * moveSpeed;
                    if (transform.position.y < (startingPosition.y - minRange))
                        currentGoingTowardsMax = true;
                }
            }

            //Going right/left
            if (elevatorDirection == DIRECTION.Right)
            {
                if (currentGoingTowardsMax == true)
                {
                    rigid.velocity = Vector3.right * moveSpeed;
                    if (transform.position.x > (startingPosition.x + maxRange))
                        currentGoingTowardsMax = false;
                }
                else
                {
                    rigid.velocity = Vector3.left * moveSpeed;
                    if (transform.position.x < (startingPosition.x - minRange))
                        currentGoingTowardsMax = true;
                }
            }

            //Going forward/back
            if (elevatorDirection == DIRECTION.Forward)
            {
                if (currentGoingTowardsMax == true)
                {
                    rigid.velocity = Vector3.forward * moveSpeed;
                    if (transform.position.z > (startingPosition.z + maxRange))
                        currentGoingTowardsMax = false;
                }
                else
                {
                    rigid.velocity = Vector3.back * moveSpeed;
                    if (transform.position.z < (startingPosition.z - minRange))
                        currentGoingTowardsMax = true;
                }
            }
        }
        if (!turnedOn)
        {
            rigid.velocity = Vector3.zero;
            //Debug.Log("Not moving.");
        }
    }
}
