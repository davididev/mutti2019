﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    public AudioClip onCompleteSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rot = transform.eulerAngles;
        rot.y += 360f * Time.deltaTime;
        transform.eulerAngles = rot;
    }

    private bool touchRoutine = false;
    IEnumerator GoalTouched()
    {
        touchRoutine = true;
        AudioSource.PlayClipAtPoint(onCompleteSound, transform.position);
        yield return new WaitForSeconds(onCompleteSound.length);
        LoadingUI.SceneIDToLoad = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene("Loading");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && touchRoutine == false)
        {
            StartCoroutine(GoalTouched());
        }
    }
}
