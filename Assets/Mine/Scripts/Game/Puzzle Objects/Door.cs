﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private Animator anim;
    public AudioClip openDoor, closeDoor;
    public bool openOnTurnOn = true;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        if (openOnTurnOn == false)  //Open in the beginning
            Open();
    }

    void Open()
    {
        anim.SetTrigger("Open");
        AudioSource.PlayClipAtPoint(openDoor, transform.position);
    }

    void Close()
    {
        anim.SetTrigger("Close");
        AudioSource.PlayClipAtPoint(closeDoor, transform.position);
    }

    public void TurnOn()
    {
        if (openOnTurnOn)
            Open();
        else
            Close();
    }

    public void TurnOff()
    {
        if (openOnTurnOn)
            Close();
        else
            Open();
    }
    
}
