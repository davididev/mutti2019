﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform headRotator;
    private CharacterController cc;
    private CharacterControllerMovement ccm;
    public GameObject pauseMenu;

    public static bool disablePause = false;
    // Start is called before the first frame update
    void Start()
    {
        disablePause = SceneManager.GetActiveScene().buildIndex < 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (cc == null)
            cc = GetComponent<CharacterController>();
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();
		
        Vector3 rot = transform.eulerAngles;
        rot.y = HeadRotator.RealFacing;
        transform.eulerAngles = rot;

        Vector3 top = transform.position + cc.center + (Vector3.up * cc.height * 0.4f);
        headRotator.transform.position = top;
        /*
        Vector2 moveVec = Vector2.zero;
        if (Finch.FinchController.Right.SwipeTop)
            moveVec.y += 1f;
        if (Finch.FinchController.Right.SwipeBottom)
            moveVec.y -= 1f;
        if (Finch.FinchController.Right.SwipeRight)
            moveVec.x += 1f;
        if (Finch.FinchController.Right.SwipeLeft)
            moveVec.x -= 1f;
        */
		//Begin controls
		if(PlayerHand.isWorking == true)
		{
			Vector2 moveVec = Finch.FinchController.Right.TouchAxes;
			moveVec.x = Mathf.Round(moveVec.x * 2f) / 2f;
			moveVec.y = Mathf.Round(moveVec.y * 2f) / 2f;

			ccm.moveVec = moveVec;



			//Pause mode
			if (disablePause == false)
			{
				if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.AppButton))
				{
					//Toggle pause
					if (Mathf.Approximately(Time.timeScale, 1f))
					{
						//Pause
						Time.timeScale = 0f;
						PlayerHand.uiMode = true;
						pauseMenu.SetActive(true);
					}
					else
					{
						//Unpause
						Time.timeScale = 1f;
						PlayerHand.uiMode = false;
						pauseMenu.SetActive(false);
					}
				}
			}
		}
		//End controls


    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        ControllerCollisionMessage msg = new ControllerCollisionMessage(gameObject, hit);
        hit.gameObject.SendMessage("ControlHit", msg, SendMessageOptions.DontRequireReceiver);
    }
}
