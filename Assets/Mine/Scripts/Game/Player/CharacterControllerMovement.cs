﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CharacterControllerMovement : MonoBehaviour
{
    private const float DEFAULT_GRAVITY = 32.15223f;
    public CharacterController cc;
    public Vector2 moveVec;
    public float minSpeed = 1f, maxSpeed = 3.5f, movementPerSecond = 6f, gravityScale = 1f, waterVertical = 0f;
    private float forwardSpeed, rightSpeed = 0f;
    public float timeInWater { private set; get; }

    public bool ignoreGravity = false, noMovementInAir = true;

    public enum ACT_IN_WATER { FLOAT, SINK };
    public ACT_IN_WATER waterAct = ACT_IN_WATER.FLOAT;

    public class CCForce
    {
        private const float MAX_TIME = 0.5f;
        private float time = 0f;
        private Vector3 force;

        /// <summary>
        /// Create a force, as it sounds
        /// </summary>
        /// <param name="f">The vector of the force</param>
        public void CreateForce(Vector3 f)
        {
            force = f;
            time = MAX_TIME;
        }

        /// <summary>
        /// Utility function called per step.
        /// </summary>
        /// <returns></returns>
        public Vector3 ForcePerSecond()
        {
            time -= Time.deltaTime;
            if(time < 0f)
            {
                time = 0f;
                return Vector3.zero;
            }
            Vector3 v = force * (time / MAX_TIME);
            v = v * Time.deltaTime;
            //Debug.Log("Force: " + v);
            return v;
        }

        /// <summary>
        /// Checks to see if the force is empty.
        /// </summary>
        /// <returns></returns>
        public bool isEmpty()
        {
            if (time <= 0f)
                return true;
            else
                return false;
        }
    }

    private CCForce[] forces = new CCForce[6];


    // Start is called before the first frame update
    void Start()
    {
        timeInWater = 0f;
        if (cc == null)
            cc = GetComponent<CharacterController>();

        for(int i = 0; i < forces.Length; i++)
        {
            forces[i] = new CCForce();
        }
    }
    float gravity = 0f;
    // Update is called once per frame
    void Update()
    {
        float gs = gravityScale;  //When we have a global gravity scale, put it here.
        if (cc == null)
            cc = GetComponent<CharacterController>();

        //Process forces
        Vector3 overallForce = Vector3.zero;
        for(int i = 0; i < forces.Length; i++)
        {
            overallForce += forces[i].ForcePerSecond();
        }
        float headY = cc.transform.position.y + (cc.height * 0.45f);
        //Process gravity
        if (cc.isGrounded)
        {
            if(waterAct == ACT_IN_WATER.FLOAT)
                gravity = 0.1f * gs;
        }
        else
            gravity += DEFAULT_GRAVITY * Time.deltaTime * gs;
        overallForce += (Vector3.down * gravity * Time.deltaTime);

    
        //Process movement
        if (moveVec.y != 0f)
        {
            forwardSpeed += (moveVec.y * movementPerSecond * Time.deltaTime);
            if (Mathf.Abs(forwardSpeed) < minSpeed)
            {
                if (forwardSpeed < 0f)
                    forwardSpeed = -minSpeed;
                else
                    forwardSpeed = minSpeed;
            }
            if (Mathf.Abs(forwardSpeed) > maxSpeed)
            {
                if (forwardSpeed < 0f)
                    forwardSpeed = -maxSpeed;
                else
                    forwardSpeed = maxSpeed;
            }

        }
        else
            forwardSpeed = Mathf.MoveTowards(forwardSpeed, 0f, movementPerSecond * 2f * Time.deltaTime);
        if (moveVec.x != 0f)
        {
            rightSpeed  += (moveVec.x * movementPerSecond * Time.deltaTime);
            if (Mathf.Abs(rightSpeed) < minSpeed)
            {
                if (rightSpeed < 0f)
                    rightSpeed = -minSpeed;
                else
                    rightSpeed = minSpeed;
            }
            if (Mathf.Abs(rightSpeed) > maxSpeed)
            {
                if (rightSpeed < 0f)
                    rightSpeed = -maxSpeed;
                else
                    rightSpeed = maxSpeed;
            }

        }
        else
            rightSpeed = Mathf.MoveTowards(rightSpeed, 0f, movementPerSecond * 2f * Time.deltaTime);

        overallForce += (transform.forward * forwardSpeed * Time.deltaTime) + (transform.right * rightSpeed * Time.deltaTime);


        cc.Move(overallForce);

    }

    public void AddForce(Vector3 v)
    {
        for(int i = 0; i < forces.Length; i++)
        {
            if(forces[i].isEmpty())
            {
                forces[i].CreateForce(v);
                return;
            }
        }

        Debug.LogWarning("Could not add force.  :(");
    }
}
