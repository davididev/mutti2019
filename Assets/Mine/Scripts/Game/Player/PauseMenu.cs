﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RestartLevel()
    {
        int sceneToLoad = SceneManager.GetActiveScene().buildIndex;
        LoadingUI.SceneIDToLoad = sceneToLoad;
        SceneManager.LoadScene("Loading");

    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void ReturnToMenu()
    {
        //Add code later
        LoadingUI.SceneIDToLoad = 0; //Load title
        SceneManager.LoadScene("Loading");
    }
}
