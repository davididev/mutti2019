﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueHandler : MonoBehaviour
{
    [SerializeField] private AudioSource source;
    [SerializeField] private TMPro.TextMeshProUGUI textObj, timeText;
    private Coroutine c;
    public static bool IS_RUNNING = false;
    // Start is called before the first frame update
    void Start()
    {
        textObj.text = "";
        IS_RUNNING = false;   //In case we've changed scenes.
        StartCoroutine(ShowTime());
    }

    IEnumerator ShowTime()
    {
        while(gameObject)
        {
            System.DateTime current = System.DateTime.Now;
            int h = current.Hour;
            string amOrPM = "AM";
            if (current.Hour == 0)
                h = 12;  //12 AM
            if (current.Hour > 12)
                h = current.Hour - 12;
            if(current.Hour >= 12)
                amOrPM = "PM";
            timeText.text = h + ":" + current.Minute.ToString("D2") + " " + amOrPM;
            yield return new WaitForSecondsRealtime(1f);
        }
    }

    public void StartDialogue(TextAsset asset)
    {
        if (IS_RUNNING)
            StopCoroutine(c);
        c = StartCoroutine(Dia(asset.text));
    }

    IEnumerator Dia(string s)
    {
        IS_RUNNING = true;
        string[] lines = s.Split('\n');
        for(int i = 0; i < lines.Length; i++)
        {
            string[] args = lines[i].Split(';');
			if(args[1].Contains("#D"))
			{
				args[1] = args[1].Replace("#D1", "<sprite=\"Diagrams\" index=0>\n");
				args[1] = args[1].Replace("#D2", "<sprite=\"Diagrams\" index=1>\n");
				args[1] = args[1].Replace("#D3", "<sprite=\"Diagrams\" index=2>\n");
				args[1] = args[1].Replace("#D4", "<sprite=\"Diagrams\" index=3>\n");
			}
            AudioClip clip = Resources.Load<AudioClip>("Audio/" + args[0]);
            source.clip = clip;
            source.Play();
            textObj.text = args[1];
            while (source.isPlaying == true)
            {
                yield return new WaitForEndOfFrame();
            }

        }
        textObj.text = "";
        IS_RUNNING = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
