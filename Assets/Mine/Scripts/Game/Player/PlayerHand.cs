﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerHand : MonoBehaviour
{
    public static bool uiMode = false;
    public static bool isWorking = false;
    public Camera uiCamera;
    private Animator anim;
    private Transform cubeGrabbed;

    public Transform pointerFinger;
    public GameObject handMesh, targetSprite;
    private int uiLayerMask, pausedUICullingMask, unpausedUICullingMask;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        isWorking = true;
        uiLayerMask = LayerMask.GetMask("UI", "UI2");
        pausedUICullingMask = LayerMask.GetMask("UI", "Player Hand");
        unpausedUICullingMask = LayerMask.GetMask("UI");
    }

    private void OnDisable()
    {
        isWorking = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (anim == null)
            anim = GetComponent<Animator>();

        uiCamera.cullingMask = (uiMode == true) ? pausedUICullingMask : unpausedUICullingMask;

        anim.SetBool("Pointing", uiMode);
        if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
        {
            anim.SetBool("Grab", true);
            if (cubeGrabbed != null)
            { 
                cubeGrabbed.transform.parent = transform;
                cubeGrabbed.GetComponent<Rigidbody>().useGravity = false;
            }
        }

        if (Finch.FinchController.Right.GetPressUp(Finch.FinchControllerElement.Trigger))
        {
            anim.SetBool("Grab", false);
            if(cubeGrabbed != null)
            {
                Release();
            }
        }

        targetSprite.SetActive(uiMode);
        if (uiMode == true)
        {
            //Raycast
            //handMesh.layer = LayerMask.NameToLayer("UI");
            RaycastHit hitInfo;
            if(Physics.Raycast(pointerFinger.position, pointerFinger.up, out hitInfo, 20f, uiLayerMask))
            {
                

                GameObject g = hitInfo.collider.gameObject;
                Button b = g.GetComponent<Button>();
                if (b != null)
                {
                    EventSystem.current.SetSelectedGameObject(g);

                    if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
                    {
                        Debug.Log("Button pressed!");

                        b.onClick.Invoke();
                    }
                }
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
            float d = hitInfo.distance;
            if (Mathf.Approximately(d, 0f))
                d = 15;
            targetSprite.transform.position = pointerFinger.position + (pointerFinger.up * (d- 0.5f));
            targetSprite.transform.LookAt(Camera.main.transform.position);
            //Vector3 start = pointerFinger.position;
            //Vector3 end = pointerFinger.position + (pointerFinger.up * 45f);
        }
        else
        {
            //handMesh.layer = LayerMask.NameToLayer("Player Hand");
        }

    }

    /// <summary>
    /// Release the cube
    /// </summary>
    void Release()
    {
        if (cubeGrabbed != null)
        {
            cubeGrabbed.transform.parent = null;
            Rigidbody rg = cubeGrabbed.GetComponent<Rigidbody>();
            rg.useGravity = true;
            //rg.AddForce(Finch.FinchController.Right.AngularVelocity * 1.2f, ForceMode.Impulse);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Cube" && cubeGrabbed == null)
        {
            other.GetComponent<CCube>().OpenTrigger();
            cubeGrabbed = other.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Cube")
        {
            other.GetComponent<CCube>().CloseTrigger();
            Release();
            cubeGrabbed = null;
        }
    }
}
