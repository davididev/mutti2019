﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelVars : MonoBehaviour
{
    public AudioClip levelMusic;
    public TextAsset startingDialogue;

    private AudioSource musicSrc;
    // Start is called before the first frame update
    void Start()
    {
        if(levelMusic != null)
            InitMusic();

        if (startingDialogue != null)
            StartCoroutine(StartDia());
    }

    IEnumerator StartDia()
    {
        yield return new WaitForSeconds(0.25f);
        while(PlayerHand.isWorking == false)
        {
            yield return new WaitForEndOfFrame();
        }
        FindObjectOfType<DialogueHandler>().StartDialogue(startingDialogue);
    }

    void InitMusic()
    {
        musicSrc = Camera.main.gameObject.AddComponent<AudioSource>();
        musicSrc.volume = 0.5f;
        musicSrc.clip = levelMusic;
        musicSrc.minDistance = 5f;
        musicSrc.loop = true;
        musicSrc.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
