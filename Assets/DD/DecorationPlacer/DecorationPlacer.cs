﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorationPlacer : MonoBehaviour {

    [System.Serializable]
    public struct PoolItem
    {
        public GameObject prefab;
        public int weight;
        public float minScale;
        public float maxScale;
        public Vector3 randomRotation;

    }

    [SerializeField]
    public PoolItem[] possibleDecorations;
    public LayerMask placeOnThese;

    //Settings
    [HideInInspector] public bool rotateToNormal = false;
    [HideInInspector] public float distanceFromSurface = 0f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
